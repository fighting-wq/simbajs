$.extend({
    BASE_URL: 'http://59.110.172.128:3636',
    ajaxHack(option) {
        $.ajax({
            url: this.BASE_URL + option.url,
            type: option.type || 'get',
            data: option.data,
            success: option.success
        })
    },
    getHack(url, data, success) {
        this.log("请求了" + url)
        $.get(this.BASE_URL + url, data, function (data) {
            layer.closeAll('loading');
            success(data)
        })
    },
    getQueryArgs() {
        var queryStr = location.search.replace('?', '')
        var obj = {}
        queryStr.split('&').forEach(item => {
            var kvStr = item.split('=')
            var key = kvStr[0]
            var val = kvStr[1]
            obj[key] = val
        })

        return obj
    },
    setCookie(key, val, expires) {
		var str = `${key}=${val}`
		if (expires) {
			var date = new Date()
			date.setDate(date.getDate() + expires)
			str += ';expires=' + date.toUTCString()
		}

		document.cookie = str
	},
	getCookie(key) {
		var arr = document.cookie.split('; ')
		for (let i = 0; i < arr.length; i++) {
			var kvStr = arr[i].split('=')
			if (key === kvStr[0])
				return kvStr[1]
		}
	},
	removeCookie(key) {
		this.setCookie(key, null, -1)
	},
    setStorage(key, val, expires) {
        var now = new Date()
        now.setMinutes(now.getMinutes() + expires)
        var obj = {
            data: val,
            expires: now.toUTCString()
        }
        localStorage.setItem(key, JSON.stringify(obj))
    },
    getStorage(key) {
        var cache = localStorage.getItem(key)
        if (!cache) return null
        var obj = JSON.parse(cache)
        if (new Date() > new Date(obj.expires)) {
            localStorage.removeItem(key)
            return null
        }
        return obj.data
    },
    l() {
        console.log(...arguments)
    },
    get2Cache(url, data, success, key, expires = 60 * 24) {
        let cache = $.getStorage(key)
        if (!cache) {
            // console.log('没有缓存，去后台')
            this.getHack(url, data, res => {
                $.setStorage(key, res, expires)
                success(res)
            })
        } else {
            // console.log("从缓存取")
            success(cache)
        }
    }
})
